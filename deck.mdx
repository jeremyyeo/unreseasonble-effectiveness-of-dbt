import { CodeSurfer, CodeSurferColumns, Step } from "code-surfer";
import { github, vsDark, oceanicNext } from "@code-surfer/themes";

export const theme = oceanicNext;

## The unreasonable effectiveness of dbt

An introduction to building analytical data warehouses 🗃️

<span style="font-size: 55%; opacity: 0.6; margin-top: 20%">
  Use the arrow keys ( ⬅️ ➡️ ) on your keyboard to navigate this deck
</span>

---

## Metadata and lineage

<img width="80%" src="lineage.png" />

---

<CodeSurferColumns>

<Step title="OLAP vs OLTP">

```sql subtitle="✅"
SELECT SUM(amount)
  FROM sales
 WHERE color = 'blue';

SELECT COUNT(DISTINCT customer_id)
  FROM customer
 WHERE location = 'au'
   AND created_at >= '1970-01-01';
```

```sql subtitle="❌"
UPDATE customer
   SET location = 'nz'
 WHERE customer_id = 'alice';

INSERT INTO sales
       (customer_id, amount)
VALUES ('alice', 100),
       ('bob', 200),
       ('eve', 300);
```

</Step>

</CodeSurferColumns>

---

Row-oriented `[("alice", "nz", 100), ("bob", "au", 200)]`

<img src="row-oriented.gif" />

Columnar `[("alice", "bob"), ("nz", "au"), (100, 200)]`

<img src="col-oriented.gif" />

---

### Data build tool (dbt)

- Open source tool (3.2k ⭐)
- Compiles and run SQL + Jinja
- Builds searchable documentation
- `pip install dbt`
- `dbt <command>`

---

<CodeSurferColumns>

<Step title="Profiles">

```yaml subtitle="~/.dbt/profiles.yml"
default:
  outputs:
    prod:
      type: postgres
      host: 127.0.0.1
      port: 5432
      user: user
      pass: pass
      dbname: postgres
      schema: public
target: prod
```

```bash subtitle="shell"
dbt run
```

</Step>

<Step title="Profiles">

```yaml subtitle="~/.dbt/profiles.yml"
default:
  outputs:
    prod:
      type: postgres
      host: 127.0.0.1
      port: 5432
      user: user
      pass: pass
      dbname: postgres
      schema: public
    test:
      type: postgres
      host: 127.0.0.2
      port: 5432
      user: user
      pass: pass
      dbname: postgres
      schema: public
target: test
```

```bash subtitle="shell"
dbt run # equivalent: dbt run --target test
dbt run --target prod
```

</Step>

<Step title="Profiles">

```yaml subtitle="~/.dbt/profiles.yml"
default:
  outputs:
    prod:
      type: postgres
      host: 127.0.0.1
      port: 5432
      user: user
      pass: pass
      dbname: postgres
      schema: public
    test:
      type: postgres
      host: 127.0.0.2
      port: 5432
      user: user
      pass: pass
      dbname: postgres
      schema: public
    analytics:
      type: snowflake
      account: 123.ap-southeast-2.snowflakecomputing.com
      user: user
      private_key_path: ~/.ssh/my_priv_key
      dbname: analytics
      schema: marts
target: test
```

```bash subtitle="shell"
dbt run # equivalent: dbt run --target test
dbt run --target prod
dbt run -t analytics
```

</Step>

</CodeSurferColumns>

---

<CodeSurferColumns sizes={[2, 2, 1]}>

<Step title="Models">

```sql subtitle="models"
-- my_model.sql
SELECT 1 AS num
```

```shell subtitle="shell"
dbt run
```

```markup subtitle="dag"
┌──────────────┐
│   my_model   │
└──────────────┘
```

</Step>

<Step title="Models">

```sql subtitle="models"
-- my_model.sql
SELECT customer_id
       , created_at
       , location
       , birth_month
  FROM customer

-- model_2.sql
SELECT *
  FROM {{ ref('my_model') }}
 WHERE created_at >= '1970-01-01'
```

```shell subtitle="shell"
dbt run
```

```markup 1:8 subtitle="dag"
┌──────────────┐
│   my_model   │
└──────────────┘
        │
        ▼
┌──────────────┐
│   model_2    │
└──────────────┘
```

</Step>

<Step title="Models">

```sql subtitle="models"
-- my_model.sql
SELECT customer_id
       , created_at
       , location
       , birth_month
  FROM {{ source('public', 'customer') }}

-- model_2.sql
SELECT *
  FROM {{ ref('my_model') }}
 WHERE created_at >= '1970-01-01'

-- metric.sql
SELECT COUNT(*) AS my_kpi
  FROM {{ ref('model_2') }}
 WHERE location = 'au'
```

```shell subtitle="shell"
dbt run
```

```markup 1:13 subtitle="dag"
┌──────────────┐
│   my_model   │
└──────────────┘
        │
        ▼
┌──────────────┐
│   model_2    │
└──────────────┘
        │
        ▼
┌──────────────┐
│    metric    │
└──────────────┘
```

</Step>

<Step title="Models: incremental macro">

```sql subtitle="models"
-- my_model.sql
{{ config(materialized='incremental') }}
SELECT customer_id
       , created_at
       , location
       , birth_month
  FROM {{ source('public', 'customer') }}
{% if is_incremental() %}
 WHERE created_at > (SELECT MAX(created_at) FROM {{ this }})
{% endif %}

-- model_2.sql
SELECT *
  FROM {{ ref('my_model') }}
 WHERE created_at >= '1970-01-01'

-- metric.sql
SELECT COUNT(*) AS my_kpi
  FROM {{ ref('model_2') }}
 WHERE location = 'au'
```

```shell subtitle="shell"
# initial run
dbt run
```

```markup 1:13 subtitle="dag"
┌──────────────┐
│   my_model   │
└──────────────┘
        │
        ▼
┌──────────────┐
│   model_2    │
└──────────────┘
        │
        ▼
┌──────────────┐
│    metric    │
└──────────────┘
```

</Step>

<Step title="Models: no new rows in customer table">

```sql subtitle="models"
-- my_model.sql
{{ config(materialized='incremental') }}
SELECT customer_id
       , created_at
       , location
       , birth_month
  FROM {{ source('public', 'customer') }}
{% if is_incremental() %}
 WHERE created_at > (SELECT MAX(created_at) FROM {{ this }})
{% endif %}

-- model_2.sql
SELECT *
  FROM {{ ref('my_model') }}
 WHERE created_at >= '1970-01-01'

-- metric.sql
SELECT COUNT(*) AS my_kpi
  FROM {{ ref('model_2') }}
 WHERE location = 'au'
```

```shell subtitle="shell"
# subsequent run
dbt run
```

```markup 1:13 subtitle="dag"
┌ ─ ─ ─ ─ ─ ─ ─
    my_model   │
└ ─ ─ ─ ─ ─ ─ ─
        │
        ▼
┌ ─ ─ ─ ─ ─ ─ ─
    model_2    │
└ ─ ─ ─ ─ ─ ─ ─
        │
        ▼
┌ ─ ─ ─ ─ ─ ─ ─
     metric    │
└ ─ ─ ─ ─ ─ ─ ─
```

</Step>

<Step title="Models: new rows in customer table">

```sql subtitle="models"
-- my_model.sql
{{ config(materialized='incremental') }}
SELECT customer_id
       , created_at
       , location
       , birth_month
  FROM {{ source('public', 'customer') }}
{% if is_incremental() %}
 WHERE created_at > (SELECT MAX(created_at) FROM {{ this }})
{% endif %}

-- model_2.sql
SELECT *
  FROM {{ ref('my_model') }}
 WHERE created_at >= '1970-01-01'

-- metric.sql
SELECT COUNT(*) AS my_kpi
  FROM {{ ref('model_2') }}
 WHERE location = 'au'
```

```shell subtitle="shell"
# subsequent run
dbt run

# if you ever mess anything up
dbt run --full-refresh
```

```markup 1:13 subtitle="dag"
┌──────────────┐
│   my_model   │
└──────────────┘
        │
        ▼
┌──────────────┐
│   model_2    │
└──────────────┘
        │
        ▼
┌──────────────┐
│    metric    │
└──────────────┘
```

</Step>

<Step title="Models: selecting">

```sql subtitle="models"
-- my_model.sql
{{ config(materialized='incremental') }}
SELECT customer_id
       , created_at
       , location
       , birth_month
  FROM {{ source('public', 'customer') }}
{% if is_incremental() %}
 WHERE created_at > (SELECT MAX(created_at) FROM {{ this }})
{% endif %}

-- model_2.sql
SELECT *
  FROM {{ ref('my_model') }}
 WHERE created_at >= '1970-01-01'

-- metric.sql
SELECT COUNT(*) AS my_kpi
  FROM {{ ref('model_2') }}
 WHERE location = 'au'
```

```shell subtitle="shell"
dbt run -m metric
```

```markup 1:13 subtitle="dag"
┌ ─ ─ ─ ─ ─ ─ ─
    my_model   │
└ ─ ─ ─ ─ ─ ─ ─
        │
        ▼
┌ ─ ─ ─ ─ ─ ─ ─
    model_2    │
└ ─ ─ ─ ─ ─ ─ ─
        │
        ▼
┌──────────────┐
│    metric    │
└──────────────┘
```

</Step>

<Step title="Models: selecting">

```sql subtitle="models"
-- my_model.sql
{{ config(materialized='incremental') }}
SELECT customer_id
       , created_at
       , location
       , birth_month
  FROM {{ source('public', 'customer') }}
{% if is_incremental() %}
 WHERE created_at > (SELECT MAX(created_at) FROM {{ this }})
{% endif %}

-- model_2.sql
SELECT *
  FROM {{ ref('my_model') }}
 WHERE birth_month IN ('jan', 'feb')

-- metric.sql
SELECT COUNT(*) AS my_kpi
  FROM {{ ref('model_2') }}
 WHERE location = 'au'
```

```shell subtitle="shell"
dbt run -m metric
dbt run -m model_2+
```

```markup 1:13 subtitle="dag"
┌ ─ ─ ─ ─ ─ ─ ─
    my_model   │
└ ─ ─ ─ ─ ─ ─ ─
        │
        ▼
┌──────────────┐
│   model_2    │
└──────────────┘
        │
        ▼
┌──────────────┐
│    metric    │
└──────────────┘
```

</Step>

<Step title="Models: selecting">

```sql subtitle="models"
-- my_model.sql
{{ config(materialized='incremental') }}
SELECT customer_id
       , created_at
       , location
       , birth_month
  FROM {{ source('public', 'customer') }}
{% if is_incremental() %}
 WHERE created_at > (SELECT MAX(created_at) FROM {{ this }})
{% endif %}

-- model_2.sql
SELECT *
  FROM {{ ref('my_model') }}
 WHERE birth_month IN ('jan', 'feb')

-- metric.sql
SELECT COUNT(*) AS my_kpi
  FROM {{ ref('model_2') }}
 WHERE location = 'au'
```

```shell subtitle="shell"
dbt run -m metric
dbt run -m model_2+
dbt run -m +model_2
```

```markup 1:13 subtitle="dag"
┌──────────────┐
│   my_model   │
└──────────────┘
        │
        ▼
┌──────────────┐
│   model_2    │
└──────────────┘
        │
        ▼
┌ ─ ─ ─ ─ ─ ─ ─
     metric    │
└ ─ ─ ─ ─ ─ ─ ─
```

</Step>

<Step title="Documentation and testing">

```sql subtitle="models" 7
-- my_model.sql
{{ config(materialized='incremental') }}
SELECT customer_id
       , created_at
       , location
       , birth_month
  FROM {{ source('public', 'customer') }}
{% if is_incremental() %}
 WHERE created_at > (SELECT MAX(created_at) FROM {{ this }})
{% endif %}

-- model_2.sql
SELECT *
  FROM {{ ref('my_model') }}
 WHERE birth_month IN ('jan', 'feb')

-- metric.sql
SELECT COUNT(*) AS my_kpi
  FROM {{ ref('model_2') }}
 WHERE location = 'au'
```

```yaml subtitle="docs"
# sources.yml
sources:
  - name: public
    schema: public
    tables:
      - name: customer
        columns:
          - name: customer_id
          - name: created_at
          - name: location
          - name: birth_month
```

```shell subtitle="shell"
dbt docs generate
```

</Step>

<Step title="Documentation and testing">

```sql subtitle="models" 7
-- my_model.sql
{{ config(materialized='incremental') }}
SELECT customer_id
       , created_at
       , location
       , birth_month
  FROM {{ source('public', 'customer') }}
{% if is_incremental() %}
 WHERE created_at > (SELECT MAX(created_at) FROM {{ this }})
{% endif %}

-- model_2.sql
SELECT *
  FROM {{ ref('my_model') }}
 WHERE birth_month IN ('jan', 'feb')

-- metric.sql
SELECT COUNT(*) AS my_kpi
  FROM {{ ref('model_2') }}
 WHERE location = 'au'
```

```yaml subtitle="docs"
# sources.yml
sources:
  - name: public
    schema: public
    description: "The main schema"
    tables:
      - name: customer
        description: "Table contains customer information"
        columns:
          - name: customer_id
            description: "The customer's unique identifier"
          - name: created_at
            description: "When the customer entered the database"
          - name: location
            description: "The customer's delivery location"
          - name: birth_month
            description: "The customer's birthday month"
```

```shell subtitle="shell"
dbt docs generate
```

</Step>

<Step title="Schema testing">

```sql subtitle="models" 7
-- my_model.sql
{{ config(materialized='incremental') }}
SELECT customer_id
       , created_at
       , location
       , birth_month
  FROM {{ source('public', 'customer') }}
{% if is_incremental() %}
 WHERE created_at > (SELECT MAX(created_at) FROM {{ this }})
{% endif %}

-- model_2.sql
SELECT *
  FROM {{ ref('my_model') }}
 WHERE birth_month IN ('jan', 'feb')

-- metric.sql
SELECT COUNT(*) AS my_kpi
  FROM {{ ref('model_2') }}
 WHERE location = 'au'
```

```yaml subtitle="docs"
# sources.yml
sources:
  - name: public
    schema: public
    description: "The main schema"
    tables:
      - name: customer
        description: "Table contains customer information"
        columns:
          - name: customer_id
            description: "The customer's unique identifier"
            tests:
              - not_null
              - unique
          - name: created_at
            description: "When the customer entered the database"
            tests:
              - not_null
          - name: location
            description: "The customer's delivery location"
            tests:
              - not_null
              - accepted_values:
                  - values: ["au", "nz", "sg", "hk", "my"]
          - name: birth_month
            description: "The customer's birthday month"
            tests:
              - not_null
              - accepted_values:
                  - values: ["jan", "feb", "mar", "apr"]
```

```shell subtitle="shell"
dbt test
dbt docs generate
```

</Step>

<Step title="Schema testing">

```sql 17:20 subtitle="models"
-- my_model.sql
{{ config(materialized='incremental') }}
SELECT customer_id
       , created_at
       , location
       , birth_month
  FROM {{ source('public', 'customer') }}
{% if is_incremental() %}
 WHERE created_at > (SELECT MAX(created_at) FROM {{ this }})
{% endif %}

-- model_2.sql
SELECT *
  FROM {{ ref('my_model') }}
 WHERE birth_month IN ('jan', 'feb')

-- metric.sql
SELECT COUNT(*) AS my_kpi
  FROM {{ ref('model_2') }}
 WHERE location = 'au'
```

```yaml subtitle="docs"
# sources.yml
sources:
  - name: public
    schema: public
    description: "The main schema"
    tables:
      - name: customer
        description: "Table contains customer information"
        columns:
          - name: customer_id
            description: "The customer's unique identifier"
            tests:
              - not_null
              - unique
          - name: created_at
            description: "When the customer entered the database"
            tests:
              - not_null
          - name: location
            description: "The customer's delivery location"
            tests:
              - not_null
              - accepted_values:
                  - values: ["au", "nz", "sg", "hk", "my"]
          - name: birth_month
            description: "The customer's birthday month"
            tests:
              - not_null
              - accepted_values:
                  - values: ["jan", "feb", "mar", "apr"]

# models.yml
models:
  - name: my_model
  - name: model_2
  - name: metric
    columns:
      - name: my_kpi
        tests:
          - not_null
```

```shell subtitle="shell"
dbt test -m metric
```

</Step>

<Step title="Data (business logic) tests">

```sql subtitle="models"
-- my_model.sql
{{ config(materialized='incremental') }}
SELECT customer_id
       , created_at
       , location
       , birth_month
  FROM {{ source('public', 'customer') }}
{% if is_incremental() %}
 WHERE created_at > (SELECT MAX(created_at) FROM {{ this }})
{% endif %}

-- model_2.sql
SELECT *
  FROM {{ ref('my_model') }}
 WHERE birth_month IN ('jan', 'feb')

-- metric.sql
SELECT COUNT(*) AS my_kpi
  FROM {{ ref('model_2') }}
 WHERE location = 'au'

-- tests/assert_my_kpi_is_positive.sql
SELECT *
  FROM {{ ref('metric') }}
 WHERE my_kpi < 0
```

```yaml subtitle="docs"
# sources.yml
sources:
  - name: public
    schema: public
    description: "The main schema"
    tables:
      - name: customer
        description: "Table contains customer information"
        columns:
          - name: customer_id
            description: "The customer's unique identifier"
            tests:
              - not_null
              - unique
          - name: created_at
            description: "When the customer entered the database"
            tests:
              - not_null
          - name: location
            description: "The customer's delivery location"
            tests:
              - not_null
              - accepted_values:
                  - values: ["au", "nz", "sg", "hk", "my"]
          - name: birth_month
            description: "The customer's birthday month"
            tests:
              - not_null
              - accepted_values:
                  - values: ["jan", "feb", "mar", "apr"]

# models.yml
models:
  - name: my_model
  - name: model_2
  - name: metric
    columns:
      - name: my_kpi
        tests:
          - not_null
```

```shell subtitle="shell"
dbt test -m metric

dbt test -m test_type:data

dbt test -m test_type:schema

dbt test
```

</Step>

</CodeSurferColumns>

---

<code>;</code>

**Jeremy Yeo**

[www.picturatechnica.com](https://www.picturatechnica.com/)
